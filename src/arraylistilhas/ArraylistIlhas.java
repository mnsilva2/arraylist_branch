/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraylistilhas;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author TPSI
 */
public class ArraylistIlhas {

    static ArrayList<Integer> ids = new ArrayList<>();
    static ArrayList<String> Nomes = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int opcao = -1;
        String nome;
        int id;
        int posicao;
        boolean unico;
        mostrarMenu();
        while (opcao != 0) {

            try {
                System.out.println("Escolha uma opção        9-Menu");
                opcao = sc.nextInt();
                sc.nextLine();
                switch (opcao) {
                    case 1:
                        System.out.println("Indique o nome da ilha que quer adicionar");
                        nome = sc.nextLine();
                        unico = true;
                        for (int i = 0; i < Nomes.size(); i++) {
                            if (nome.equals(Nomes.get(i))) {
                                System.out.println("A ilha que inseriu ja existe");
                                unico = false;
                            }

                        }
                        if (!unico) {
                            break;
                        }
                        if (ids.isEmpty()) {
                            ids.add(1);
                        } else {
                            ids.add(ids.get(ids.size() - 1) + 1);
                        }
                        Nomes.add(nome);
                        System.out.println("Adicionado com sucesso");

                        break;
                    case 2:
                        System.out.println("Indique o ID da ilha que quer remover");
                        id = sc.nextInt();
                        sc.nextLine();
                        posicao = ids.indexOf(id);
                        if (posicao == -1) {
                            System.out.println("Esse Id nao existe");
                            break;
                        }
                        ids.remove(posicao);
                        Nomes.remove(posicao);
                        ids.trimToSize();
                        Nomes.trimToSize();
                        System.out.println("Removido com sucesso");

                        break;
                    case 3:
                        System.out.println("Indique o ID da ilha que quer Editar");
                        id = sc.nextInt();
                        sc.nextLine();
                        posicao = ids.indexOf(id);
                        if (posicao == -1) {
                            System.out.println("Esse Id nao existe");
                            break;
                        }
                        System.out.println("Indique o novo nome");
                        nome = sc.nextLine();
                        Nomes.set(posicao, nome);
                        System.out.println("Editado com sucesso");

                        break;
                    case 4:
                        mostrarIlha();
                        break;
                    case 5:
                        System.out.println("Indique o nome ou parte do nome da(s) ilha(s) que que procurar");

                        mostrarIlhaNome(sc.nextLine());

                        break;
                    case 6:
                        System.out.println("Indique o nome ou parte do Id da(s) ilha(s) que que procurar");

                        mostrarIlhaID(sc.nextInt());
                        sc.nextLine();

                        break;
                    case 7:
                        System.out.println("Indique o nome da ilha que quer adicionar escrevar \"exit\" para parar de inserir");
                        while (true) {
                            nome = sc.nextLine();
                            if (nome.equals("exit")) {
                                break;
                            }
                            unico = true;
                            for (int i = 0; i < Nomes.size(); i++) {
                                if (nome.equals(Nomes.get(i))) {
                                    System.out.println("A ilha que inseriu ja existe");
                                    unico = false;
                                }

                            }
                            if (!unico) {
                                break;
                            }

                            if (ids.isEmpty()) {
                                ids.add(1);
                            } else {
                                ids.add(ids.get(ids.size() - 1) + 1);
                            }
                            Nomes.add(nome);
                            System.out.println("Adicionado com sucesso");
                        }

                        break;
                    case 9:
                        mostrarMenu();
                        break;
                    case 0:

                        break;
                    default:
                        System.out.println("Opçãoo Errada");

                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Deve ter inserido alguma coisa mal");
                opcao = -1;
                sc = new Scanner(System.in);
            }
        }
        System.out.println("Byeeeeeee");

    }

    private static void mostrarIlha() {
        System.out.println("**********************************");
        System.out.println("* IDS \t| Nomes");

        for (int i = 0; i < ids.size(); i++) {
            System.out.println("* ------|---------");
            System.out.println("* " + ids.get(i) + "\t| " + Nomes.get(i));

        }
        System.out.println("**********************************");

    }

    private static void mostrarMenu() {

        System.out.println("**********************************");
        System.out.println("* 1- Adicionar uma ilha          *");
        System.out.println("* 2- Remover uma ilha            *");
        System.out.println("* 3- Editar uma ilha             *");
        System.out.println("* 4- Mostrar ilhas               *");
        System.out.println("* 5- Mostrar ilha por Nome       *");
        System.out.println("* 6- Mostrar ilha por ID         *");
        System.out.println("* 7- Adicionar varias ilhas      *");
        System.out.println("* 9- Mostrar Menu                *");
        System.out.println("* 0- Sair                        *");
        System.out.println("**********************************");
    }

    private static void mostrarIlhaNome(String s) {
        System.out.println("**********************************");
        System.out.println("* IDS \t| Nomes");

        for (int i = 0; i < ids.size(); i++) {
            if (Nomes.get(i).contains(s)) {
                System.out.println("* ------|---------");
                System.out.println("* " + ids.get(i) + "\t| " + Nomes.get(i));
            }
        }
        System.out.println("**********************************");
    }

    private static void mostrarIlhaID(int id) {
        System.out.println("**********************************");
        System.out.println("* IDS \t| Nomes");

        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i).toString().contains(id + "")) {
                System.out.println("* ------|---------");
                System.out.println("* " + ids.get(i) + "\t| " + Nomes.get(i));
            }
        }
        System.out.println("**********************************");
    }

}
